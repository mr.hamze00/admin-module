<?php

namespace Modules\Admin\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class updateConfigAuth extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin-panel:update-config-auth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update config\'s auth';

    protected $files;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $setting = [
            'path' => '/config/auth.php',

            'search-guard' => "'guards' => [",
            'stub-guard' => module_path('Admin', 'stubs/config/auth-guard.stub'),


            'search-provider' => "'providers' => [",
            'stub-provider' => module_path('Admin', 'stubs/config/auth-provider.stub'),
            'search-password' => "'passwords' => [",
            'stub-password' => module_path('Admin', 'stubs/config/auth-password.stub'),

        ];

        $fullPath = base_path() . $setting['path'];

        $originalContent = $this->files->get($fullPath);

        $content_guard = $this->files->get($setting['stub-guard']);
        $content_provider = $this->files->get($setting['stub-provider']);
        $content_password = $this->files->get($setting['stub-password']);

        $stub_guard = $setting['search-guard'] . $content_guard;
        $stub_provider = $setting['search-provider'] . $content_provider;
        $stub_password = $setting['search-password'] . $content_password;

        $originalContent = str_replace($setting['search-guard'], $stub_guard, $originalContent);
        $originalContent = str_replace($setting['search-provider'], $stub_provider, $originalContent);
        $originalContent = str_replace($setting['search-password'], $stub_password, $originalContent);

        $this->files->put($fullPath, $originalContent);
    }


}
