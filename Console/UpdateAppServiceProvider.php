<?php

namespace Modules\Admin\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class updateAppServiceProvider extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin-panel:update-app-service-provider';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update AppServiceProvider';

    protected $files;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $setting = [
            'path' => '/app/Providers/AppServiceProvider.php',
            'search' => "/public function boot\(\)\n    {\n        \/\//",
            'orginal' => "public function boot()",
            'stub' => module_path('Admin', 'stubs/Providers/AppServiceProvider.stub'),

            'search_imports' => 'use Illuminate\Support\ServiceProvider;',
            'stub_imports' => module_path('Admin', 'stubs/Providers/AppServiceProviderImports.stub'),
        ];

        $fullPath = base_path() . $setting['path'];

        $originalContent = $this->files->get($fullPath);
        $content = $this->files->get($setting['stub']);
        $stub = $setting['orginal'] . $content;
        $originalContent = preg_replace($setting['search'], $stub, $originalContent);

        $content_imports = $this->files->get($setting['stub_imports']);
        $stub_imports = $setting['search_imports'] . $content_imports;
        $originalContent = str_replace($setting['search_imports'], $stub_imports, $originalContent);

        $this->files->put($fullPath, $originalContent);
    }


}
