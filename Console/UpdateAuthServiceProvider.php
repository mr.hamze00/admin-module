<?php

namespace Modules\Admin\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class updateAuthServiceProvider extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin-panel:update-auth-service-provider';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update authServiceProvider and add SuperAdmin';

    protected $files;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $setting = [
            'path' => '/app/Providers/AuthServiceProvider.php',
            'search' => '$this->registerPolicies();',
            'stub' => module_path('Admin', 'stubs/Providers/AuthServiceProvider.stub'),

        ];

        $fullPath = base_path() . $setting['path'];

        $originalContent = $this->files->get($fullPath);
        $content = $this->files->get($setting['stub']);
        $stub = $setting['search'] . $content;

        $originalContent = str_replace($setting['search'], $stub, $originalContent);

        $this->files->put($fullPath, $originalContent);
    }


}
