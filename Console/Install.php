<?php

namespace Modules\Admin\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class install extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin-panel:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install and update settings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call('admin-panel:update-middleware-kernel');
        Artisan::call('admin-panel:update-auth-service-provider');
        Artisan::call('admin-panel:update-app-service-provider');
        Artisan::call('admin-panel:update-config-auth');
        Artisan::call('admin-panel:update-console-kernel');
    }


}
